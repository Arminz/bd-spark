from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS

from pyspark.sql import SparkSession
from pyspark.sql.types import *


spark = SparkSession.builder.appName('recommendation system').getOrCreate()

sc = spark.sparkContext


schema = StructType([
    StructField('userId', IntegerType()),
    StructField('movieId', IntegerType()),
    StructField('rating', DoubleType()),
    StructField('timestamp', LongType()),
])

lines = spark.read.option("header", "true").schema(schema).csv("data/ml-latest-small/ratings.csv")

(training, test) = lines.randomSplit([0.8, 0.2])

als = ALS(maxIter=5, regParam=0.01, userCol="userId", itemCol="movieId", ratingCol="rating", coldStartStrategy="drop")
model = als.fit(training)

predictions = model.transform(test)
evaluator = RegressionEvaluator(metricName="rmse", labelCol="rating", predictionCol="prediction")
rmse = evaluator.evaluate(predictions)
print("Root-mean-square error = " + str(rmse))

model = als.fit(lines)
print("fitted on all the data")

# Generate top 10 movie recommendations for each user
userRecs = model.recommendForAllUsers(10)

user_id = int(input('enter user id: '))

movie_rates = userRecs.filter(userRecs['userId'] == user_id).toPandas()['recommendations'][0]
movie_recommends = []
for movie_rate in movie_rates:
    movie_recommends.append(movie_rate[0])

print('movies recommended for user {}: '.format(movie_recommends))



